# Video assets for MLSS 2020

Assets for a short video presentation of what we do in the C3 project of the SFB 876. The result is on YouTube: https://youtu.be/mnuCUZMXvUM


## Reproducing the assets

Run `make` in the `tex` directory to reproduce all animations and overlays.

The other assets (videos and voiceovers) are not contained in the repo, due to their sizes.


## Sources

file | licence | copyright owner | source
--- | --- | --- | ---
`asset/tex/nsb.jpg` | CC0 | Felix Mittermeier | https://pxhere.com/en/photo/1441911
`asset/fact-datataking.mp4` | all rights reserved | Maximilian Nöthe | https://www.flickr.com/photos/maximus_cr/22565058320/in/photostream/
`asset/fact-drivetest.mp4` | all rights reserved | Maximilian Nöthe | https://www.flickr.com/photos/maximus_cr/22377816927/in/photostream/
`asset/roque-de-los-muchachos.mp4` | all rights reserved | Maximilian Nöthe | https://www.flickr.com/photos/maximus_cr/22500911370/in/photostream/
`asset/background-music.mp3` | creative commons | Royalty Free Music | https://soundcloud.com/royaltyfreebackgroundmusic/creative-commons-music-epic-5


---

## Title and abstract

**The Many Ways of Learning from Astro-Particle Simulations**

My intention is not to provide an in-depth presentation of some particular learning task, but to invite you to discuss with me the many tasks involved in analyzing data from astro-particle telescopes. First of all, why do we need these telescopes and why is machine learning important to them? Since all labeled data is obtained from simulations, how do we deal with data quality issues? How can we reduce the resource footprint of the analysis pipeline? Answers to these questions embrace imbalanced learning, domain adaptation, active class selection, deep learning, and probability calibration, among others.


## Manuscript

The following parts are in the order of their appearance. *Voiceover* parts are those that are dubbed over an animated video. *Talk* parts refer to those parts where I stand in front of the camera.

### Voiceover A

Contemporary telescopes allow us to probe the universe with a tremendous coverage of the electro-magnetic spectrum.
Here, you can see FACT, an Imaging Air Cherenkov telescope.
As such, it measures the most energetic part of the electromagnetic spectrum - gamma particles from distant galaxies.

### Talk A

Welcome, youtube!

You are watching my submission to this year's Machine Learning Summer School.
Let me introduce you to the machine learning aspects involved in running a telescope like FACT,
and how my colleagues and I approach them.
My name is Mirko Bunse and I am a research associate at TU Dortmund University.

### Voiceover B

First of all, how can a gamma particle be detected by a ground-based telescope?
A particle that hits the atmosphere will start to interact and produce secondary particles. These secondary particles interact further, resulting in a chain of interactions that is called an 'extensive air shower'.
Since this shower emits Cherenkov light, it can be recorded with a specialized camera mounted onto a telescope.

### Voiceover C

However, just having recorded the Cherenkov light, we have not yet characterized the gamma particle that started the shower.
Therefore, we are feeding the data into a machine learning system, which is able to reconstruct the primary particle from the measurements.
It would not be feasible to do this reconstruction by hand because the raw data can easily have a volume of 1TB per night, per telescope.

### Talk B

So which properties of the primary particle do we need to reconstruct?
Let's take a look at the gamma-ray sky.

### Voiceover D

The celestial sources of gamma radiation are not yet completely understood, so that precise measurements of their emission are crucial.
One thing these sources emit are charged particles, which are deflected by magnetic fields.
Due to this deflection, you never know where these particles originate from, so that they do not contain any useful information.
Unfortunately, they produce extensive air showers as well, so that a Cherenkov telescope might mistake a charged particle for a gamma-ray.
We must learn to distinguish this noise from the actual gamma-ray signal we want to measure.
One particular difficulty is that the noise events outnumber the signal events by 1000:1, or even worse.

### Voiceover E

Gamma-rays, to the contrary, point right back to their source.
Once we are sure that the telescope has seen one, we must reconstruct it's precise direction and it's energy.
Ultimately, the probability density of particle energies will help physicists to better understand gamma-ray sources.
All of these prediction tasks are tackled by supervised machine learning.

### Talk C

The essential requirement for supervised learning is of course the training data.
In Cherenkov astronomy, this data must come from simulations because the telescope recordings are never labeled.

### Voiceover EE or Talk CC

Building accurate simulations is a huge effort;
and even the most accurate simulation is but a model of reality.
When learning from simulations, we must ensure that the gap between the simulated and the real world is appropriately bridged.
Any action towards this goal can be understood as domain adaptation.

### Voiceover F

At TU Dortmund University, we are particularly concerned with reducing the resource footprint of the data analysis.
For example, we can save time and energy by aborting individual simulation runs that are not informative.
Active class selection might even be able to not even start these runs.

### Voiceover G

For a large array of telescopes, which is yet to come, we already presented a feature extraction pipeline that works in real-time.
We are also following the trend of deep learning,
which can replace the costly extraction of hand-crafted features.
Our deep models can even be deployed on field-programmable gate arrays,
which have a tremendously small resource footprint.

### Voiceover H

The final step of the analysis is to aggregate the events recorded during an observation.
Since the uncertainty of predictions is essential at this step,
we must calibrate all predictions probabilistically.

### Talk D

I hope you enjoyed this short introduction in Cherenkov astronomy.
Let's discuss *your* ideas on the topic during the Q&A session of the summer school,
or in the comments below.

Stay safe!
