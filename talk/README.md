part | video | audio (white background)
---- | ----- | -----
A    | 00025 | 027
A    | 00027 | 028
B    | 00029 | 029
B    | 00030 | 030
C    | 00031 | 031
D    | 00032 | 032

part | video | audio (office background)
---- | ----- | -----
A    | 00036 | 033
B    | 00037 | 034
B    | 00038 | 035
C    | 00039 | 036
D    | 00040 | 037

### 1) extract audio from videos

    make mts-to-ac3

### 2) Audacity

- import the corresponding `.MTS.AC3` and `.WAV` file
- increase the speed of the `.WAV` recording: *Effect - Change Speed...* and specify *Percent Change: 0.006*.
- move the `.WAV` recording over the `.MTS.AC3` recording - keep the AC3 track fixed!
- normalize to -1dB.
- apply distortion (value of 8 in *Even Harmonics*; see mlss preset)
- export the `.WAV` track to `raw/%.MTS.wav`.

### 3) merge audio with video

    make join-video-audio



### XXX) OpenShot

- drag the *Brightness & Contrast* effect **twice** on each of the talk videos.
- the first effect needs *Brightness: -0,1* and *Contrast: 0,0*.
- the second effect needs *Contrast: 18,0*
