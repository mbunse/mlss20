\documentclass{asset}

\input{include/air-shower} % provides the command \iactSchemeFrame
\input{include/fact-data} % provides the command \factDataFrame

\tikzset{every node/.append style={%
  font=\huge\sffamily\bfseries,
  inner sep=5pt
}}

\tikzstyle{telescope} = [anthracite, line width=2pt]

% telescope
\newcommand{\telescopeFrame}[1]{%
  \edef\j{#1} % make sure #1 (===\pgfmathresult) is not overwritten
  \pgfmathparse{6*cos(2*\j)-21}
  \edef\telescopeRotation{\pgfmathresult}

  \begin{scope}[shift={(28pt,16pt)}, scale = .25]
    \draw[telescope] (0.0,-0.35) -- (-0.3,-1);
    \draw[telescope] (0.2,-0.35) -- (0.5,-1);

    \begin{scope}[shift={(.1,-.3)}]
    \begin{scope}[rotate=\telescopeRotation]
    \begin{scope}[shift={(-.5,.12)}]
      \draw[telescope] (0.15,-0.1) -- (0.4,0.55);
      \draw[telescope] (0.85,-0.1) -- (0.6,0.55);

      \coordinate (CAM) at (0.55,0.65);
      % \node[left=1.1, yshift = -2.5] (telescope-label) at (CAM) {\color{black}{telescope}};

      \draw[telescope, fill=lightgray] (0.4,0.45) -- (0.6,0.45) -- (0.6,0.7) -- (0.4,0.7) -- cycle;
      \draw[telescope, fill=lightgray] (0,0) to[out=300,in=240] (1,0) -- cycle;
      \draw[telescope] (0,0) to[out=300,in=240] (1,0) -- (0,0);
      % \fill[red] (.5,-.12) circle[radius=2pt]; % pivot point for rotation
    \end{scope}
    \end{scope}
    \end{scope}
  \end{scope}
} % end of \newcommand{\telescopeFrame}

% Cherenkov light
\newcommand{\cherenkovLightFrame}[1]{%
  \ifthenelse{#1 < 8}{%
    \pgfmathparse{max(0, min(#1/8, 1))}
  }{%
    \pgfmathparse{.66+.17*(1+cos((#1-8)*.08 r))}
  }
  \edef\topLightOpacity{\pgfmathresult} % opacity of the top ellipse

  \ifthenelse{#1 < 16}{%
    \pgfmathparse{max(0, min((#1-8)/8, 1))}
  }{%
    \pgfmathparse{.66+.17*(1+cos((#1-16)*.08 r))}
  }
  \edef\middleLightOpacity{\pgfmathresult} % opacity of the middle ellipse

  \ifthenelse{#1 < 24}{%
    \pgfmathparse{max(0, min((#1-16)/8, 1))}
  }{%
    \pgfmathparse{.66+.17*(1+cos((#1-24)*.08 r))}
  }
  \edef\bottomLightOpacity{\pgfmathresult} % opacity of the bottom ellipse

  % draw the ellipses
  \begin{scope}[shift = {(58pt,24pt)}, xscale = .45, yscale = .25]
      \fill[fill=tu04midlight!50,rotate=-10, opacity=\topLightOpacity] (-1.5,1.1) ellipse (0.45 and 0.25);
      \fill[fill=tu04midlight!75,rotate=-10, opacity=\middleLightOpacity] (-1.625,0.55) ellipse (0.5 and 0.25);
      \fill[fill=tu04midlight!90,rotate=-10, opacity=\bottomLightOpacity] (-1.75,0) ellipse (0.55 and 0.25);
      \coordinate (cherenkov light label) at (-.75,.66);
  \end{scope}
} % end of \newcommand{\cherenkovLightFrame}

% main frame
\newcommand{\cherenkovAstronomyFrame}[1]{%
  
  % opacity of labels (fading out during the view shift)
  \pgfmathparse{max(0, min(1-(#1-500)/25, 1))} % the fading happens in frames 500--525
  \edef\frameLabelOpacity{\pgfmathresult}
  \tikzstyle{frame label}=[font=\tiny\sffamily\bfseries, scale=.66, transform shape, opacity=\frameLabelOpacity]

  \begin{tikzpicture}[scale=10, transform shape]
  \begin{scope}[scale=1.0036883230955485]

    % atmosphere and ground
    \coordinate (scene north west) at (-25pt,72pt); % -24 with 1pt margin
    \coordinate (scene north east) at (136pt,72pt); % 128 + 8
    \coordinate (scene south west) at (-25pt,0pt);
    \coordinate (scene south east) at (136pt,0pt);
    \pgfmathsetlengthmacro{\viewShift}{min(8, max(0, (#1-500)*1.33)-24)}
    \path[use as bounding box] (\viewShift,0pt) rectangle ++(128pt,72pt); % with scale=1.0036883230955485 + scale=10 and 72dpi, these are 1280x720px
    \node[rotate=.001*#1+.396, shift={(.0005*#1-.24, -.00025*#1+.18)}] at (current bounding box.center) {\includegraphics[scale=.15]{include/nsb.jpg}};
    \fill[black, opacity=.42] (current bounding box.south west) rectangle (current bounding box.north east);
    \fill[tu03light!75, path fading=l2r, fading angle=90] (scene south west) rectangle (scene north east);
    \coordinate (ground north west) at ([yshift=8pt] scene south west);
    \coordinate (ground north east) at ([yshift=6pt] scene south east);
    \fill[tu01!60] (scene south east) -- (scene south west) -- (ground north west) to[out=175,in=8] (ground north east) -- cycle;
    \draw[tu01, line width=3pt] (ground north west) to[out=175,in=8] (ground north east);

    \telescopeFrame{#1}

    % air shower (must stand before the primary particle, even though animated later)
    \pgfmathparse{int((#1-175)*.15)} % multiply to change speed
    \edef\kFirstReactionOffset{\pgfmathresult}
    \pgfmathparse{int((#1-260)*1.5)}
    \edef\kShowerOffset{\pgfmathresult}
    \airShowerFrame{\kFirstReactionOffset}{\kShowerOffset}
    \ifthenelse{\kShowerOffset > 50 \AND #1 < 525}{
      \node[frame label, shower, left, align=center]
        at (secondary particle label) {air shower\\\scalebox{.8}{(secondary}\\[-.2em]\scalebox{.8}{particles)}};
    }{}

    % primary particle
    \ifthenelse{\k > 125}{%
      \ifthenelse{\k < 130}{%
        \draw[draw until=(\k-125)/5, ->, tu06, line width=4pt, dash pattern=on 12pt off 8pt]
          ([shift={(6pt,12pt)}] shower-start) -- ([shift={(1pt,2pt)}] shower-start);
      }{%
        \draw[->, tu06, line width=4pt, dash pattern=on 12pt off 8pt]
          ([shift={(6pt,12pt)}] shower-start) -- ([shift={(1pt,2pt)}] shower-start)
          coordinate[pos=.5, xshift=3pt] (primary particle label);
        \ifthenelse{\k < 525}{%
          \node[frame label, tu06, right] at (primary particle label) {primary particle};
        }{}
      }
    }{}

    % Cherenkov light
    \pgfmathparse{int(#1-437)}
    \edef\kCherenkovLightOffset{\pgfmathresult}
    \cherenkovLightFrame{\kCherenkovLightOffset}
    \ifthenelse{\k > 450 \AND \k < 525}{% % =437+12
      \node[frame label, tu04light, right] at (cherenkov light label) {Cherenkov light};
    }{}

    % FACT data
    \pgfmathparse{int(#1-533)}
    \edef\kFactDataOffsetTwo{\pgfmathresult}
    \factDataFrameTwo{\kFactDataOffsetTwo}

    \pgfmathparse{int(#1-650)}
    \edef\kFactDataOffsetOne{\pgfmathresult}
    \factDataFrameOne{\kFactDataOffsetOne}

  \end{scope}
  \end{tikzpicture}
} % end of \newcommand{\cherenkovAstronomyFrame}

% animate
\begin{document}
\foreach \k in {-25,...,800} {%
  \cherenkovAstronomyFrame{\k}
}%
\end{document}
