#!/bin/bash
#
# USAGE: convert-pdf <input>.pdf <output, including a "%" for the sequence number>.jpg
set -e

# to be used in a pipe (see below)
input=$1
output=$2
extract_page () {
	target="${output/\%/$1}"
	convert -density 108 $input[$1] $target && echo $target
}

# exports required for GNU parallel
export input
export output
export -f extract_page

n_pages=$(pdftk $input dump_data | awk '/NumberOfPages/{print $2}')
seq -w 00000 $(printf "%05d" $(($n_pages-1))) \
  | parallel extract_page \
  | tqdm --total $n_pages --ncols 60 > /dev/null
