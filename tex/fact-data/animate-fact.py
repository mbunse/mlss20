import argparse, matplotlib, logging
import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
from fact.plotting import camera
from matplotlib.animation import FuncAnimation
from mpl_toolkits.axes_grid1 import make_axes_locatable
from tqdm import trange

TIKZ_HEADER = """
\\newcommand{\\showFactDataFrame}[4][]{%
  \\ifthenelse{#2}{%
    \\node[#1] (fact data frame) at (0,0) {\\includegraphics[width=68pt]{#4}}
      node[font=\\tiny\\sffamily\\bfseries, anchor=north east, shift={(38pt,35pt)}] {#3};
  }{}
}

\\newcommand{\\factDataFrame}[2]{%
  \\begin{scope}[shift = {(96pt,36pt)}]
"""
TIKZ_FOOTER = """
  \\end{scope}
} % end of \\newcommand{\\showFactDataFrame}
"""

def main(fits_file, event_nr, outpath_template, slices=np.arange(6, 191, 2)):
    """Export one pdf for each frame in an event.

    Note: My slide set used slices=np.arange(40, 141, 20).
    """
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
    logging.info('Exporting event nr %d from %s', event_nr, fits_file)
    data = Table.read(fits_file)['DataCalibrated'][event_nr].reshape(1440, 300)

    # prepare figure (omit colorbar)
    fig = plt.figure(figsize=(10, 10), dpi=150)
    ax = fig.add_subplot(1, 1, 1)
    ax.set_axis_off()
    plot = camera(
        np.zeros(1440),
        ax=ax,
        vmin=data[:, slices].min(),
        vmax=data[:, slices].max(),
        cmap='inferno'
    )
    fig.tight_layout()

    # vector graphics export
    for i in trange(len(slices), ncols=60):
        sl = slices[i]
        timeseries = data[:, sl]
        plot.set_array(timeseries)
        outpath = outpath_template % i
        plt.savefig(outpath, transparent=True)

    # print tikz code to console
    fmt_str = "    \\showFactDataFrame[]{{\\kFactDataOffset = {}}}{{{}}}{{fact-data/{}-{}.png}}"
    print(TIKZ_HEADER)
    print("\n".join([fmt_str.format(i, '{}ns'.format(int(slices[i] / 2)), event_nr, "{:03d}".format(i)) for i in range(len(slices))]))
    print(TIKZ_FOOTER)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('fits_file')
    parser.add_argument('event_nr', type=int, choices=np.arange(4))
    parser.add_argument('outpath_template')
    args = parser.parse_args()
    main(args.fits_file, args.event_nr, args.outpath_template)

#
# MP4 video export (original script)
#
# def update(i):
#     sl = slices[i]
#     timeseries = data[:, sl]
# 
#     plot.set_array(timeseries)
#     title.set_text('$t = {:4.1f}$ ns'.format(sl / 2))
# 
#     return plot, title,
# 
# ani = FuncAnimation(fig, update, blit=False, frames=len(slices), interval=1000, repeat=False)
# ani.save('fact.png', 'ffmpeg')
#
