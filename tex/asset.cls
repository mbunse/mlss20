\RequirePackage{luatex85}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{asset}

\LoadClass[tikz]{standalone} % [border=5pt]

% encoding & language
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}

% tikz
\RequirePackage{tikz,pgfplots}
\pgfplotsset{compat=1.15}
\usetikzlibrary{arrows,arrows.meta,backgrounds,calc,fadings,fit,patterns,positioning,shadows}
\input{include/colors}
\input{include/styles}

% other packages
\RequirePackage{ifthen}

% new features
\input{include/animate-line}

% TU-like font
\RequirePackage[no-math]{fontspec}
\setsansfont[
  Ligatures = TeX,
  BoldFont = FiraSans,
  ItalicFont = FiraSans-LightItalic,
  BoldItalicFont = FiraSans-Italic
]{FiraSans-Light}
\newfontfamily{\bxseries}{FiraSans-SemiBold}[
  LetterSpace=3.0,
  WordSpace=1.33,
  ItalicFont = FiraSans-SemiBoldItalic
]
% \usefonttheme[onlymath]{serif}
